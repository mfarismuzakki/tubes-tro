﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class PlayerStats : MonoBehaviour {

    public int fullHp = 100;
    public int currentHp;
    public Slider healthSlider;
    bool isDamaged;
    bool isDead;
    public int statusPlayer;
    CharacterMovement characterMovement;
 

	// Use this for initialization
	void Start () {
        if (statusPlayer == 1)
        {
            characterMovement = GetComponent<CharacterMovement>();
         
        }

        currentHp = fullHp;
    }
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKeyDown(KeyCode.Space))
        {
            takeDamage(10);
        }

        if(currentHp <= 0)
        {
            Death();
        }
	}

    public void cekAttack(int damage)
    {
        if(hitung() == 1)
        {
            GameObject enemy = GameObject.FindGameObjectWithTag("Enemy1");
            var attack = enemy.GetComponent<PlayerStats>();
            attack.takeDamage(damage);
        }
        else
        {
            GameObject player = GameObject.FindGameObjectWithTag("Player");
            var attack = player.GetComponent<PlayerStats>();
            attack.takeDamage(damage);
        }
    }

    public void takeDamage(int damage)
    {
        currentHp -= damage;
        healthSlider.value = (float)currentHp / 100;
    }

    void Death()
    {
        //status 1 = player
        if(statusPlayer == 1)
        {
            SceneManager.LoadScene(1);
        }
        else
        {
            GameObject enemy = GameObject.FindGameObjectWithTag("Enemy1");

            enemy.GetComponent<Enemy>().back();

            Destroy(enemy);
        }
    }


    public InputField z1;
    public InputField z2;
    public InputField zHasil;


    public InputField x11;
    public InputField x21;
    public InputField x1Hasil;


    public InputField x12;
    public InputField x22;
    public InputField x2Hasil;

    public InputField x13;
    public InputField x23;
    public InputField x3Hasil;

    private string tmatriks;

    public int hitung()
    {
        //penampung array ke string
        string tmatriks = "";
        int bMasalah = 3;

        float[,] tmpMatriks = new float[bMasalah + 2, bMasalah + 4];


        //isi 0 untuk semua matriks
        for (int i = 0; i < bMasalah + 2; i++)
        {
            for (int j = 0; j < bMasalah + 4; j++)
            {
                tmpMatriks[i, j] = 0;
            }
        }


        //memasukan input kedalam array
        tmpMatriks[1, 0] = float.Parse(x11.text);
        tmpMatriks[1, 1] = float.Parse(x21.text);
        tmpMatriks[1, 2] = 1;
        tmpMatriks[1, bMasalah + 2] = float.Parse(x1Hasil.text); ;

        tmpMatriks[2, 0] = float.Parse(x12.text); ;
        tmpMatriks[2, 1] = float.Parse(x22.text); ;
        tmpMatriks[2, 3] = 1;
        tmpMatriks[2, bMasalah + 2] = float.Parse(x2Hasil.text); ;

        tmpMatriks[3, 0] = float.Parse(x13.text); ;
        tmpMatriks[3, 1] = float.Parse(x23.text); 
        tmpMatriks[3, 4] = 1;
        tmpMatriks[3, bMasalah + 2] = float.Parse(x3Hasil.text); ;

        tmpMatriks[0, 0] = tmpMatriks[3, 0];
        tmpMatriks[0, 1] = tmpMatriks[3, 1];
        tmpMatriks[0, bMasalah + 2] = tmpMatriks[3, bMasalah + 2];


        //isi tanda
        //isi input matriks
        tmpMatriks[1, bMasalah + 3] = -1000;
        tmpMatriks[2, bMasalah + 3] = -2000;
        tmpMatriks[3, bMasalah + 3] = 3000;
        tmpMatriks[bMasalah + 1, 0] = 100;
        tmpMatriks[bMasalah + 1, 1] = 200;
        tmpMatriks[bMasalah + 1, 2] = -1000;
        tmpMatriks[bMasalah + 1, 3] = -2000;
        tmpMatriks[bMasalah + 1, 4] = 3000;


        //fase 1
        tmatriks += "FASE KE 1 \n\n";

        //tanda iterasi ke 1
        //print iterasi ke 0
        tmatriks += "Iterasi ke 0 \n";
        for (int i = 0; i < bMasalah + 2; i++)
        {
            for (int j = 0; j < bMasalah + 4; j++)
            {
                tmatriks += tmpMatriks[i, j] + "\t";
            }
            tmatriks += "\n";
        }
        tmatriks += "\n";

        //penampung mantriks sebelum ditukar
        float[,] sebelumDituker = new float[bMasalah + 2, bMasalah + 4];
        for (int i = 0; i < bMasalah + 2; i++)
        {
            for (int j = 0; j < bMasalah + 4; j++)
            {
                sebelumDituker[i, j] = tmpMatriks[i, j];
            }
        }

        //fase ke 1 **************************************
        int status = 0;
        int increment = 1;

        //selama masih bisa increment
        while (status == 0 && increment < 15)
        {
            //deklarasi variabel
            float max = -999; // penampung nilai terbesar
            int col = -9;  //penampung index colom dengan nilai terbesar
            int bar = -9; //penampung index baris yang akan ditukar / nilai solusi terbesar
            float idenBar = -9; // penampung nilai baris inti
            float idenbar2 = -9; // penampung nilai kolom inti

            //proses pencarian kolom yang masih belum - / 0 pada baris z
            for (int i = 0; i < bMasalah + 2; i++)
            {
                //jika dari kolom baris z ada yang lebih besar dari max
                if (tmpMatriks[0, i] > max)
                {
                    max = tmpMatriks[0, i]; // pemindahan variabel ke tmp max
                    col = i;
                    idenbar2 = tmpMatriks[bMasalah + 1, i];
                }
            }


            //mencari yang baris ditukar
            if (max > 0)
            {
                float min = 9999;
                float[] solusi = new float[bMasalah + 2];

                //mencari solusi dengan nilai terendah
                for (int i = 1; i < bMasalah + 2; i++)
                {
                    solusi[i] = tmpMatriks[i, bMasalah + 2] / tmpMatriks[i, col];
                    if (min > solusi[i] && solusi[i] > 0)
                    {
                        min = solusi[i];
                        bar = i;
                        idenBar = tmpMatriks[i, bMasalah + 3];
                    }
                }
            }

            //penampung nilai yang akan ditukar
            int idenTuker = 0;

            float[] tampungTuker = new float[bMasalah + 1];
            for (int i = 0; i < bMasalah + 2; i++)
            {
                if (tmpMatriks[bMasalah + 1, i] == idenBar)
                {
                    idenTuker = i;
                    for (int j = 1; j < bMasalah + 1; j++)
                    {
                        tampungTuker[j] = tmpMatriks[j, i];
                    }
                }
            }

            tmpMatriks[bar, bMasalah + 3] = idenbar2; //identitas

            //penukaran pertama
            for (int i = 1; i < bMasalah + 1; i++)
            {
                tmpMatriks[i, idenTuker] = tmpMatriks[i, col];
            }

            //penukaran kedua
            for (int i = 1; i < bMasalah + 1; i++)
            {
                tmpMatriks[i, col] = tampungTuker[i];
            }


            //penampung matriks baru
            float[,] matriksBaru = new float[bMasalah + 1, bMasalah + 3];

            //pengisian nilai sementara matriks baru
            for (int i = 0; i < bMasalah + 1; i++)
            {
                for (int j = 0; j < bMasalah + 3; j++)
                {
                    matriksBaru[i, j] = -999;
                }
            }

            //pemindahan kolom utama
            float pembagiBu = -9;
            for (int i = 1; i < bMasalah + 1; i++)
            {
                matriksBaru[i, col] = tmpMatriks[i, col];
                if (tmpMatriks[i, col] == 1)
                {
                    pembagiBu = sebelumDituker[i, col];
                    for (int j = 0; j < bMasalah + 3; j++)
                    {
                        if (j != col)
                        {
                            matriksBaru[i, j] = sebelumDituker[i, j] / pembagiBu;
                        }
                    }
                }
            }

            //isi matriks baru di obe
            for (int i = 0; i < bMasalah + 1; i++)
            {
                float pengurang = 0;
                for (int j = 0; j < bMasalah + 3; j++)
                {
                    if (i != bar)
                    {
                        matriksBaru[i, j] = sebelumDituker[i, j] - (matriksBaru[bar, j] * sebelumDituker[i, col]);
                    }
                }
            }

            //pemindahan ke tmpMatriks
            for (int i = 0; i < bMasalah + 1; i++)
            {
                for (int j = 0; j < bMasalah + 3; j++)
                {
                    tmpMatriks[i, j] = matriksBaru[i, j];
                }
            }

            //pemindahan ke sebelum nuker
            for (int i = 0; i < bMasalah + 2; i++)
            {
                for (int j = 0; j < bMasalah + 4; j++)
                {
                    sebelumDituker[i, j] = tmpMatriks[i, j];
                }
            }

            //pengecekan apakah lolos fase 1
            int flag = 0;
            for (int i = 0; i < bMasalah + 2; i++)
            {
                if (tmpMatriks[0, i] > 0)
                {
                    flag++;
                }
            }

            if (flag == 0)
            {
                status = 1;
            }


            //print iterasi ke n
            tmatriks += "Iterasi ke : " + increment + "\n";
            for (int i = 0; i < bMasalah + 2; i++)
            {
                for (int j = 0; j < bMasalah + 4; j++)
                {
                    tmatriks += tmpMatriks[i, j] + "\t";
                }
                tmatriks += "\n";
            }
            increment += 1;
            tmatriks += "\n";
        }
        Debug.Log(tmatriks);
        Debug.Log(status);
        return status;
    }








}
