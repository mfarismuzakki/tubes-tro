﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitStone : MonoBehaviour {

    GameObject player;
    public GameObject popUp;

    private void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player");

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == player)
        {
            popUp.SetActive(true);
            Debug.Log("kena");
        }
    }
}
