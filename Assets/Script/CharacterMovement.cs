﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovement : MonoBehaviour {

    Animator animator;
    float tmp;

    private void Start()
    {
        animator = GetComponentInChildren<Animator>();
    }


    // Update is called once per frame
    void Update () {
        var x = Input.GetAxis("Horizontal") * Time.deltaTime * 150.0f;
        var z = Input.GetAxis("Vertical") * Time.deltaTime * 3.0f;
        float speed = 0;

        transform.Rotate(0, x, 0);
        transform.Translate(0, 0, z);
        if(tmp != z)
        {
            speed = 0.5f;
        }
       
        animator.SetFloat("SpeedPercent", speed, .1f, Time.deltaTime);

        tmp = z;
    }
}
