﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

    GameObject player;
    PlayerStats playerStats;
    CharacterMovement characterMovement;
    bool kena;
    public GameObject soal;
    public Camera mainCamera;
    public Camera secondCamera;
    public GameObject enemyStats;
    public int statusEnemy;
    PlayerStats DeadEnemy;



    private void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        playerStats = player.GetComponent<PlayerStats>();
        characterMovement = player.GetComponent<CharacterMovement>();
        kena = false;

        DeadEnemy = GameObject.FindGameObjectWithTag("Enemy1").GetComponent<PlayerStats>();
       
    }



    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject == player)
        {
            soal.SetActive(true);
            characterMovement.enabled = false;
            mainCamera.gameObject.SetActive(false);
            secondCamera.gameObject.SetActive(true);
            enemyStats.SetActive(true);
        }
    }

    public void back()
    {
        soal.SetActive(false);
        characterMovement.enabled = true;
        mainCamera.gameObject.SetActive(true);
        secondCamera.gameObject.SetActive(false);
        enemyStats.SetActive(false);
    }


}
