﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class animatorController : MonoBehaviour {

    public Animator anim;
    public GameObject instruksi;

	// Use this for initialization
	void Start () {
        anim = GetComponent<Animator>();
        instruksi = GameObject.FindGameObjectWithTag("Instruksi");
    }
	
	public void Back()
    {
        anim.Play("OutHintGame");
        Invoke("destroyInstruksi",.9f);
        //Destroy(instruksi);
    }

    public void destroyInstruksi()
    {
        Destroy(instruksi);
    }



    public void Enter()
    {
        
    }

}
